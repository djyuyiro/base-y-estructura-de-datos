USE ejercicio_02

//*CL�USULA WHERE*//

//*1. Encuentre a todos los miembros del personal cuyo nombre empiece por 'H'.*//

select apellido
from plantilla
where upper(apellido) like 'H%';

//*2. Quienes son las enfermeras y enfermeros que trabajan en turnos de Tarde o Ma�ana?*//

select apellido
from plantilla
where upper(funcion) in ('ENFERMERO' ,'ENFERMERA')

and upper(turno) in ('T','M');

//*3. Haga un listado de las enfermeras que ganan entre 2.000.000 y 2.500.000 Pts.*//

select apellido, salario
from plantilla
where salario between 2000000 and 2500000
and upper(funcion)= 'ENFERMERA';

//*4. Mostrar, para todos los hospitales, el c�digo de hospital, el nombre completo del hospital
y su nombre abreviado de tres letras (a esto podemos llamarlo ABR) Ordenar la recuperaci�n por
esta abreviatura.*//

SELECT hospital_cod, nombre, SUBSTRING(nombre, 1, 3) AS abr
FROM hospital
ORDER BY abr;

//* 5. En la tabla DOCTOR otorgar a Cardiolog�a un c�digo de 1, a Psiquiatr�a un c�digo de 2,
a Pediatr�a un c�digo de 3 y a cualquier otra especialidad un c�digo de 4. Recuperar todos los
doctores, su especialidad y el c�digo asignado.*//

SELECT apellido, especialidad,
  CASE especialidad
    WHEN 'Cardiolog�a' THEN '1'
    WHEN 'Psiquiatr�a' THEN '2'
    WHEN 'Pediatr�a' THEN '3'
    ELSE '4'
  END AS codigo
FROM DOCTOR;


//*6. Hacer un listado de los nombres de los pacientes y la posici�n de la primera letra 'A' que
aparezca en su apellido, tomando como referencia la primera letra del mismo*//

SELECT apellido, CHARINDEX('A', apellido) AS "PRIMERA LETRA A"
FROM enfermo;



//*7.Queremos conseguir:
COMENTARIO
--------------------
El departamento de CONTABILIDAD esta en SEVILLA
El departamento de INVESTIGACI�N esta en MADRID
El departamento de VENTAS esta en BARCELONA
El departamento de PRODUCCI�N esta en BILBAO*//

SELECT CONCAT('El departamento de ', dnombre, ' est� en ', loc) AS COMENTARIO
FROM dept2;



//*8. Para cada empleado cuyo apellido contenga una "N", queremos que nos devuelva "nnn",
pero solo para la primera ocurrencia de la "N". La salida debe estar ordenada por apellido
ascendentemente.*//

SELECT
  CASE
    WHEN CHARINDEX('N', apellido, 1) > 0 THEN
      SUBSTRING(apellido, 1, CHARINDEX('N', apellido, 1) - 1) + 'nnn' + SUBSTRING(apellido, CHARINDEX('N', apellido, 1) + 1, LEN(apellido))
    ELSE apellido
  END AS "TRES N"
FROM emp
WHERE UPPER(apellido) LIKE '%N%'
ORDER BY apellido;

//*9. Para cada empleado se pide que salga su salario total (salario mas comisi�n) y luego su
salario fragmentado, es decir, en centenas de mil, decenas de mil... decenas y unidades. La salida
debe estar ordenada por el salario y el apellido descend�ntemente.*//

SELECT
  apellido,
  salario + COALESCE(comision, 0) AS sal_total,
  FLOOR((salario + COALESCE(comision, 0)) / 100000) AS c,
  FLOOR(((salario + COALESCE(comision, 0)) % 100000) / 10000) AS d,
  FLOOR(((salario + COALESCE(comision, 0)) % 10000) / 1000) AS m,
  FLOOR(((salario + COALESCE(comision, 0)) % 1000) / 100) AS c1,
  FLOOR(((salario + COALESCE(comision, 0)) % 100) / 10) AS d1,
  (salario + COALESCE(comision, 0)) % 10 AS u
FROM emp
ORDER BY sal_total DESC, apellido DESC;

//*10. Para cada empleado que no tenga comisi�n o cuya comisi�n sea mayor que el 15% de su salario, se pide el salario total que tiene. Este ser�: si tiene comisi�n su salario mas su comisi�n,
y si no tiene, su salario mas su nueva comisi�n (15% del salario). La salida deber� estar ordenada por el oficio y por el salario que le queda descend�ntemente.*//

SELECT apellido AS APELLIDO, oficio AS OFICIO,
  salario + COALESCE(comision, salario * 0.15) AS "SALARIO TOTAL"
FROM emp
WHERE oficio IN ('ANALISTA', 'DIRECTOR', 'EMPLEADO', 'PRESIDENTE', 'VENDEDOR')
ORDER BY "SALARIO TOTAL" DESC;

//* 11. Encuentre a todas las enfermeras y enfermeros con indicaci�n del salario mensual de cada
uno.*//

SELECT apellido, ROUND(salario / 12, 0) AS "SALARIO MENSUAL"
FROM plantilla
WHERE UPPER(funcion) IN ('ENFERMERA', 'ENFERMERO');

//* 12. Que fecha fue hace tres semanas?*//

SELECT DATEADD(WEEK, -3, GETDATE()) AS fecha;

//* 13. Se pide el nombre, oficio y fecha de alta del personal del departamento 20 que ganan mas de
150000 ptas. mensuales.*//

SELECT
  apellido,
  oficio,
  CONVERT(NVARCHAR, fecha_alta, 106) + ' de ' + CONVERT(NVARCHAR, YEAR(fecha_alta)) + ' ' + REPLACE(CONVERT(NVARCHAR, FORMAT(fecha_alta, 'HH:mm'), 108), ':', '.') AS alta
FROM emp
WHERE dept_no = 20 AND salario > 150000;

//*14. Se pide el nombre, oficio y el d�a de la semana en que han sido dados de alta los
empleados de la empresa, pero solo de aquellos cuyo d�a de alta haya sido entre martes y jueves.
Ordenado por oficio.*//

SELECT
  emp_no,
  oficio,
  DATENAME(WEEKDAY, fecha_alta) AS dia
FROM emp
WHERE DATENAME(WEEKDAY, fecha_alta) IN ('Tuesday', 'Wednesday', 'Thursday')
ORDER BY oficio;;

//* 15. Para todos los empleados, el d�a en que fueron dados de alta en la empresa debe estar
ordenada por el d�a de la semana (Lunes, Martes ... Viernes) . Los d�as no laborables ser�n "Fin
de semana".*//

SELECT
  apellido,
  oficio,
  CASE
    WHEN DATENAME(WEEKDAY, fecha_alta) = 'Monday' THEN 'Lunes'
    WHEN DATENAME(WEEKDAY, fecha_alta) = 'Tuesday' THEN 'Martes'
    WHEN DATENAME(WEEKDAY, fecha_alta) = 'Wednesday' THEN 'Miercoles'
    WHEN DATENAME(WEEKDAY, fecha_alta) = 'Thursday' THEN 'Jueves'
    WHEN DATENAME(WEEKDAY, fecha_alta) = 'Friday' THEN 'Viernes'
    ELSE 'Fin de semana'
  END AS dia
FROM emp
ORDER BY 3;


//*CL�USULA GROUP BY
16. Encontrar el salario medio de los Analistas.*//


select avg(salario) "SALARIO MEDIO"
from emp
where upper(oficio) = 'ANALISTA';

//*17. Encontrar el salario mas alto y el salario mas bajo de la tabla de empleados, as� como la
diferencia entre ambos.*//

select max(salario) maximo, min(salario) m�nimo,
max(salario) - min(salario) diferencia
from emp;

//*18. Calcular el numero de oficios diferentes que hay, en total, en los departamentos 10 y 20
de la empresa.*//

SELECT COUNT(DISTINCT oficio) as tareas
FROM emp
WHERE dept_no IN (10, 20);

//*19. Calcular el numero de personas que realizan cada oficio en cada departamento.*//

SELECT dept_no, oficio, COUNT(*) as total_personas
FROM emp
GROUP BY dept_no, oficio
ORDER BY dept_no, oficio;

//*20. Buscar que departamentos tienen mas de cuatro personas trabajando.*//

SELECT dept_no, COUNT(*) as personas
FROM emp
GROUP BY dept_no
HAVING COUNT(*) > 4;

//*22. Se desea saber el numero de empleados por departamento que tienen por oficio el de
"EMPLEADO". La salida debe estar ordenada por el numero de departamento.*//

select dept_no, count(*)
from emp
where upper(oficio)='EMPLEADO'
group by dept_no;

//* 23. Se desea saber el salario total (salario mas comisi�n) medio anual de los vendedores de
nuestra empresa.*//

SELECT oficio, AVG(salario + ISNULL(comision, 0)) as salario_medio_anual
FROM emp
WHERE UPPER(oficio) = 'VENDEDOR'
GROUP BY oficio;

//*24. Se desea saber el salario total (salario mas comisi�n) medio anual, tanto de los empleados
como de los vendedores de nuestra empresa.*//

SELECT oficio, AVG(salario + ISNULL(comision, 0)) as salario_medio_anual
FROM emp
WHERE UPPER(oficio) IN ('VENDEDOR', 'EMPLEADO')
GROUP BY oficio;

//*25. Se desea saber para cada departamento y en cada oficio, el maximo salario y la suma total
de salarios, pero solo de aquellos departamentos y oficios cuya suma salarial supere o sea igual
que el 50% de su maximo salario. En el muestreo, solo se estudiaron a aquellos empleados que
no tienen comisi�n o la tengan menor que el 25% de su salario.*//

SELECT dept_no, oficio, SUM(salario) as suma, MAX(salario) as maximo
FROM emp
WHERE (comision is null) or (comision < 0.25 * salario)
GROUP BY dept_no, oficio
HAVING SUM(salario) >= 0.5 * MAX(salario);

//* 26. Se desea saber para cada oficio, dentro de cada a�o de alta distinto que existe en nuestra
empresa, el numero de empleados y la media salarial que tiene. Para este estudio, no se tendr� en
cuenta a los empleados que no hayan sido dados de alta en un d�a laboral. Adem�s, solo se desea
saber estos datos, de aquellos oficios y a�os que tienen mas de un empleado. La salida debe
estar ordenada por el a�o de alta y la media salarial descend�ntemente.*//

SELECT YEAR(fecha_alta) AS alta, oficio, 
       COUNT(*) AS "N1 EMPL", 
       AVG(CAST(salario AS DECIMAL(10, 2))) AS "MEDIA SALARIAL"
FROM emp
WHERE DATEPART(WEEKDAY, fecha_alta) NOT IN (1, 7)
GROUP BY YEAR(fecha_alta), oficio
HAVING COUNT(*) > 1
ORDER BY YEAR(fecha_alta), "MEDIA SALARIAL" DESC;

//*27. Se desea saber, para cada inicial de apellido que exista en la empresa (tratando solo las
iniciales consonantes), el maximo salario que tiene asociada. No se tendr� en cuenta en el
estudio a aquellos empleados que contengan en su apellido mas de una "N". La salida debe estar
ordenada por la inicial.*//

SELECT LEFT(apellido, 1) AS inicial, MAX(salario) AS maximo
FROM emp
WHERE CHARINDEX('N', apellido, CHARINDEX('N', apellido) + 1) = 0
  AND UPPER(LEFT(apellido, 1)) NOT IN ('A', 'E', 'I', 'O', 'U')
GROUP BY LEFT(apellido, 1)
ORDER BY inicial;

//*28. Se desea obtener un informe matriz como el que se presenta, en el que la coordenada
vertical hace referencia a los distintos oficios existentes en la empresa, y la coordenada
horizontal a los distintos departamentos. Los valores de la matriz, indicaran la suma de salarios
por oficio y departamento. La ultima columna indica la suma total de salarios por oficio.*//

	SELECT oficio,
		   SUM(CASE WHEN dept_no = 10 THEN salario ELSE 0 END) AS dep10,
		   SUM(CASE WHEN dept_no = 20 THEN salario ELSE 0 END) AS dep20,
		   SUM(CASE WHEN dept_no = 30 THEN salario ELSE 0 END) AS dep30,
		   SUM(CASE WHEN dept_no = 40 THEN salario ELSE 0 END) AS dep40,
		   SUM(salario) AS total
	FROM emp
	GROUP BY oficio
	ORDER BY total DESC;

//*29. Se desea saber para cada departamento y oficio, la suma total de comisiones, teniendo en
cuenta que para los empleados que no tienen comisi�n, se les asignara:
- El 10% de su salario si son del departamento 10.
- El 15% de su salario si son del departamento 20.
- El 17% de su salario si son del departamento 30.
- Cualquier otro departamento, el 5% de su salario.
No se tendr� en cuenta a los empleados que hayan sido dados de alta despu�s de 1981, ni al que
ostente el cargo de "PRESIDENTE".*//

SELECT dept_no, oficio,
  SUM(
    CASE 
      WHEN comision IS NOT NULL THEN comision
      WHEN dept_no = 10 THEN 0.1 * salario
      WHEN dept_no = 20 THEN 0.15 * salario
      WHEN dept_no = 30 THEN 0.17 * salario
      ELSE 0.05 * salario
    END
  ) AS "SUMA DE COMISIONES"
FROM emp
WHERE YEAR(fecha_alt) <= 1981 AND UPPER(oficio) != 'PRESIDENTE'
GROUP BY dept_no, oficio;

//*30.- Queremos saber el m�ximo, el m�nimo y la media salarial, de cada departamento de la
empresa.*//

SELECT 'Maximo' AS COMENTARIO, MAX(salario) AS VALOR, dept_no
FROM emp
GROUP BY dept_no
UNION ALL
SELECT 'Media' AS COMENTARIO, AVG(salario) AS VALOR, dept_no
FROM emp
GROUP BY dept_no
UNION ALL
SELECT 'M�nimo' AS COMENTARIO, MIN(salario) AS VALOR, dept_no
FROM emp
GROUP BY dept_no
ORDER BY dept_no, COMENTARIO;

//*COMBINACIONES DE TABLAS

31. Listar, a partir de las tablas EMP y DEPT2, el nombre de cada empleado, su oficio, su
numero de departamento y el nombre del departamento donde trabajan.*//

select apellido, oficio, e.dept_no, dnombre
from emp e, dept2 d
where e.dept_no = d.dept_no;

//*32. Seleccionar los nombres, profesiones y localidades de los departamentos donde trabajan
los Analistas.*//

SELECT e.apellido AS APELLIDO, e.oficio AS OFICIO, d.loc AS LOC
FROM emp e
JOIN dept2 d ON e.dept_no = d.dept_no
WHERE UPPER(e.oficio) LIKE 'ANALISTA';

//*33. Se desea conocer el nombre y oficio de todos aquellos empleados que trabajan en Madrid.
La salida deber� estar ordenada por el oficio.*//

select apellido, oficio from emp e, dept2 d
where upper(loc) = 'MADRID' and e.dept_no = d.dept_no
order by oficio;

//*34. se desea conocer cuantos empleados existen en cada departamento. Devolviendo una
salida como la que se presenta (deber� estar ordenada por el numero de empleados
descend�ntemente).*//

select e.dept_no num_dep, dnombre departamento, count(*) n1_empl
from emp e, dept2 d
where e.dept_no = d.dept_no
group by e.dept_no, dnombre
order by 3 desc;

//*35. Se desea conocer, tanto para el departamento de VENTAS, como para el de
CONTABILIDAD, su maximo, su m�nimo y su media salarial, as� como el numero de empleados
en cada departamento. La salida deber� estar ordenada por el nombre del departamento, y se
deber� presentar como la siguiente:*//

select dnombre, max(salario) maximo, min(salario) m�nimo,
avg(salario) media, count(*) n_empl
from emp e, dept2 d
where upper(dnombre) in ('VENTAS', 'CONTABILIDAD')
and e.dept_no = d.dept_no
group by dnombre
order by dnombre;

//*36. Se desea conocer el maximo salario que existe en cada sala de cada hospital, dando el
resultado como sigue:*//

select h.nombre hospital, s.nombre sala, max(salario) maximo
from sala s, plantilla p, hospital h
where h.hospital_cod = p.hospital_cod and
p.sala_cod = s.sala_cod
group by h.nombre, s.nombre;

//*37. Se desea obtener un resultado como el que aparece, en el que se presenta el numero,
nombre y oficio de cada empleado de nuestra empresa que tiene jefe, y lo mismo de su jefe
directo. La salida debe estar ordenada por el nombre del empleado.*//

select e.emp_no empleado, e.apellido nombre,
e.oficio oficio, e.dir jefe, e2.apellido nombre, e2.oficio oficio
from emp e, emp e2
where e.dir = e2.emp_no
order by 2;

//*38. Se desea conocer, para todos los departamentos existentes, el m�nimo salario de cada
departamento, mostrando el resultado como aparece. Para el muestreo del m�nimo salario, no
queremos tener en cuenta a las personas con oficio de EMPLEADO. La salida estar� ordenada
por el salario descend�ntemente.*//

SELECT d.dnombre AS "DEPARTAMENTO", MIN(e.salario) AS "M�NIMO"
FROM dept2 d
LEFT JOIN emp e ON d.dept_no = e.dept_no
WHERE UPPER(e.oficio) NOT LIKE 'EMPLEADO' OR e.oficio IS NULL
GROUP BY d.dnombre
ORDER BY 2 DESC;

//*COMBINACIONES (OUTER JOIN)
39. Se desea sacar un listado con el mismo formato del ejercicio
33 tal y como el que aparece, pero ahora tambi�n se desea sacar al jefe de la empresa como
empleado, pues en el listado del citado ejercicio no aparec�a.*//

SELECT e.emp_no AS "EMPLEADO", e.apellido AS "NOMBRE",
       e.oficio AS "OFICIO", e.dir AS "JEFE",
       COALESCE(e2.apellido, 'PRESIDENTE') AS "JEFE NOMBRE",
       e2.oficio AS "JEFE OFICIO"
FROM emp e
LEFT JOIN emp e2 ON e.dir = e2.emp_no
ORDER BY 2;

//*40. Se desea obtener un listado como el que aparece, es decir, como el del ejercicio 33, pero
obteniendo adem�s todos aquellos empleados que no son jefes de nadie en la parte de jefe, para
que se vea gr�ficamente que no tienen subordinados a su cargo.*//

SELECT e.emp_no AS "EMPLEADO", e.apellido AS "NOMBRE",
       e.oficio AS "OFICIO", e.dir AS "JEFE",
       COALESCE(e2.apellido, 'NO JEFE') AS "JEFE NOMBRE",
       COALESCE(e2.oficio, 'NO JEFE') AS "JEFE OFICIO"
FROM emp e
LEFT JOIN emp e2 ON e.dir = e2.emp_no
ORDER BY 2;

//*41. Se desea obtener un listado combinaci�n de los dos ejercicios anteriores.*//

SELECT e.emp_no AS "EMPLEADO", e.apellido AS "NOMBRE",
       e.oficio AS "OFICIO", e.dir AS "JEFE",
       COALESCE(e2.apellido, 'NO JEFE') AS "JEFE NOMBRE",
       COALESCE(e2.oficio, 'NO JEFE') AS "JEFE OFICIO",
       CASE WHEN e2.emp_no IS NOT NULL THEN 'JEFE' ELSE 'NO JEFE' END AS "ESTADO"
FROM emp e
LEFT JOIN emp e2 ON e.dir = e2.emp_no
UNION ALL
SELECT e.emp_no, e.apellido, e.oficio, e.dir,
       COALESCE(e2.apellido, 'NO JEFE'), COALESCE(e2.oficio, 'NO JEFE'),
       CASE WHEN e2.emp_no IS NOT NULL THEN 'JEFE' ELSE 'NO JEFE' END
FROM emp e
RIGHT JOIN emp e2 ON e.dir = e2.emp_no
ORDER BY "NOMBRE";

//*SUBCONSULTAS

42. Obtener el apellido, departamento y oficio de aquellos empleados que tengan un oficio
que este en el departamento 20 y que no sea ninguno de los oficios que esta en el departamento
de VENTAS.*//

SELECT apellido, dept_no, oficio
FROM emp
WHERE oficio IN (SELECT oficio FROM emp WHERE dept_no = 20)
AND oficio NOT IN (SELECT oficio FROM emp WHERE dept_no IN (SELECT dept_no FROM dept2 WHERE UPPER(dnombre) = 'VENTAS'));

//*43. Obtener el numero de empleado, numero de departamento y apellido de todos los
empleados que trabajen en el departamento 20 o 30 y su salario sea mayor que dos veces el
m�nimo de la empresa. No queremos que el oficio sea PRESIDENTE.*//

SELECT emp_no, dept_no, apellido
FROM emp
WHERE dept_no IN (20, 30)
AND salario > (SELECT 2 * MIN(salario) FROM emp);

//*44. Encontrar las personas que ganan 500.000 PTA mas que el miembro del personal de
sueldo mas alto del turno de ma�ana y que tenga el mismo trabajo que el Sr. Nu�ez.*//

select apellido, turno, funcion, salario
from plantilla
where salario > (select max(salario) + 500000

from plantilla
where upper(turno) = 'M')
and funcion in (select funcion
from plantilla
where upper(apellido) like 'NU%');

//*45. Queremos averiguar el apellido del individuo mas antiguo de la empresa.*//

SELECT TOP 1 apellido, nombre_de_la_columna_fecha
FROM nombre_de_la_tabla
ORDER BY nombre_de_la_columna_fecha;

//*46. Presentar los nombres y oficios de los empleados que tienen el mismo trabajo que
JIMENEZ.*//

select apellido, oficio
from emp
where oficio in (select oficio
from emp
where upper(apellido) = 'JIMENEZ');

//*47. Queremos conocer el apellido, oficio, salario y departamento en el que trabajan, de todos
los individuos cuyo salario sea mayor que el mayor salario del departamento 30.*//

select apellido, oficio, salario, dept_no
from emp
where salario > (select max(salario)
from emp
where dept_no = 30);

//*48. Presentar los nombres y oficios de todos los empleados del departamento 20, cuyo trabajo
sea id�ntico al de cualquiera de los empleados del departamento de VENTAS.*//

select apellido, oficio
from emp
where dept_no = 20
and upper(oficio) in (select oficio

from emp e, dept2 d
where upper(dnombre)= 'VENTAS' and
e.dept_no = d.dept_no);

//*49. Se desea obtener todos los empleados de los departamentos que no ganan ni el maximo ni el
m�nimo salarial de la empresa.*//

select apellido, oficio
from emp
where salario <> (select max(salario)
from emp)

and salario <> (select min(salario)
from emp);

//*50. Se desea obtener el maximo salario por departamento, sin tener en cuenta a aquellos
empleados cuyo apellido empieza con la inicial de alguno de los empleados que tienen el
maximo salario de alg�n departamento. Tampoco queremos obtener los datos de departamentos
con menos de 3 personas muestreadas.*//

WITH SalarioMaximoPorDepartamento AS (
    SELECT dept_no, MAX(salario) AS maximo
    FROM emp
    GROUP BY dept_no
    HAVING COUNT(*) > 2
)

SELECT MAX(e.salario) AS maximo, e.dept_no
FROM emp e
JOIN SalarioMaximoPorDepartamento s ON e.dept_no = s.dept_no
WHERE NOT EXISTS (
    SELECT 1
    FROM emp
    WHERE dept_no = e.dept_no
    AND LEFT(apellido, 1) = LEFT(
        (SELECT TOP 1 apellido
        FROM emp
        WHERE dept_no = e.dept_no
        ORDER BY salario DESC), 1)
)
GROUP BY e.dept_no;

//*51. Se desea averiguar el numero de oficios por departamento, sin tener en cuenta en el
muestreo a aquellos individuos que est�n en alguno de los departamentos que contienen
VENDEDORES. La salida de la consulta ser� como la siguiente.*//

WITH DepartamentosSinVendedores AS (
    SELECT DISTINCT dept_no
    FROM emp
    WHERE upper(oficio) LIKE 'VENDE%'
)

SELECT COUNT(DISTINCT e.oficio) AS numero, d.dept_no AS num_dep, d.dnombre AS nombre
FROM dept2 d
LEFT JOIN emp e ON d.dept_no = e.dept_no
WHERE d.dept_no NOT IN (SELECT dept_no FROM DepartamentosSinVendedores)
GROUP BY d.dept_no, d.dnombre;

//*52. Sacar con el formato que aparece abajo, el apellido departamento y sueldo del empleado
que mas gana en la empresa y del que menos.*//

select e1.dept_no dep, e1.salario maximo,
e1.apellido apellido, e2.dept_no dep,
e2.salario m�nimo, e2.apellido apellido
from emp e1, emp e2
where e1.salario in (select max(salario)
from emp)
and e2.salario in (select min(salario)
from emp);

//*53. En que departamento se dio de alta a mas empleados en Diciembre.*//

SELECT name
FROM sys.columns
WHERE object_id = OBJECT_ID('emp') AND name LIKE '%fecha%';

SELECT d.dnombre AS departamento, COUNT(*) AS cantidad_de_altas
FROM dept2 d
JOIN emp e ON d.dept_no = e.dept_no
WHERE FORMAT(e.fecha_alta, 'MM') = '12'
GROUP BY d.dnombre
HAVING COUNT(*) >= ALL (
    SELECT COUNT(*) AS cantidad_de_altas
    FROM emp
    WHERE FORMAT(fecha_alta, 'MM') = '12'
    GROUP BY dept_no
);

//*54. Se desea obtener, para cada departamento, su m�nimo y su maximo salarial. Para ello, no
se tendr� en cuenta a los empleados cuya primera letra de su apellido, coincida con la inicial del
nombre del departamento en que trabajan. Asimismo, se tendr� en cuenta a aquellos
departamentos cuya diferencia entre el maximo y el m�nimo exceda la media salarial de toda la
empresa.*//

SELECT e.dept_no, MIN(e.salario) AS m�nimo, MAX(e.salario) AS m�ximo
FROM emp e
WHERE e.apellido NOT LIKE (SELECT d.dnombre + '%' FROM dept2 d WHERE e.dept_no = d.dept_no)
GROUP BY e.dept_no
HAVING MAX(e.salario) - MIN(e.salario) > (SELECT AVG(salario) FROM emp);

//*Queremos saber el nombre de el empleado mas joven de cada departamento, as� como el
nombre de este.*//

WITH EmpleadosMasJovenes AS (
    SELECT d.dnombre AS departamento, e.dept_no AS numero, e.apellido, e.fecha_alt, 
           ROW_NUMBER() OVER (PARTITION BY e.dept_no ORDER BY fecha_alt ASC) AS ranking
    FROM emp e
    JOIN dept2 d ON e.dept_no = d.dept_no
)
SELECT departamento, numero, apellido, fecha_alt
FROM EmpleadosMasJovenes
WHERE ranking = 1;

//*56. Se desea saber el nombre, oficio y departamento del empleado que m�s gana del
departamento con la media salarial m�s alta.*//

WITH MediaSalarialPorDepartamento AS (
  SELECT dept_no, AVG(salario) AS media_salarial
  FROM emp
  GROUP BY dept_no
)
, DepartamentoMaxMedia AS (
  SELECT dept_no
  FROM MediaSalarialPorDepartamento
  WHERE media_salarial = (SELECT MAX(media_salarial) FROM MediaSalarialPorDepartamento)
)
, EmpleadoMaxSalarioEnDepartamento AS (
  SELECT e.apellido AS nombre, e.oficio, e.dept_no, e.salario,
         RANK() OVER (PARTITION BY e.dept_no ORDER BY e.salario DESC) AS ranking
  FROM emp e
)
SELECT nombre, oficio, e.dept_no AS departamento
FROM EmpleadoMaxSalarioEnDepartamento e
JOIN DepartamentoMaxMedia d ON e.dept_no = d.dept_no
WHERE ranking = 1;

//*57. Se desea obtener informaci�n sobre todos los empleados que son jefes de alguien.*//

select e1.apellido, e1.oficio, e1.dept_no
from emp e1
where exists (select *

from emp e2
where e1.emp_no = e2.dir)

order by apellido;

//*58. Recuperar el numero (empleado_no) y nombre de las personas que perciban un salario >
que la media de su hospital.*//

select apellido, empleado_no
from plantilla p
where salario > (select avg(salario)
from plantilla p2
where p2.hospital_cod = p.hospital_cod);

//*59. Insertar en la tabla Plantilla al Garcia J. con un sueldo de 3000000 ptas, y n�mero de
empleado 1234. Trabaja en el hospital 22, sala2.*//

INSERT INTO Plantilla (hospital_cod, sala_cod, empleado_no, apellido, funcion, turno, salario)
VALUES (22, 2, 1234, 'Garcia J.', 'Enfermo', 'M', 3000000);

//*60. Insertar la misma fila anterior sin indicar en que campos se insertan. )Por qu� no se
indican?*//

insert into plantilla
values (22,2,1234,'Garcia J.','Enfermero','M',3000000)

//*61. insert into plantilla
(empleado_no, apellido)
values (1234,'Garcia J)
Esta inserci�n falla. )Por qu�?.*//

INSERT INTO Plantilla (empleado_no, apellido)
VALUES (1234, 'Garcia J');

//*62. insert into plantilla
(hospital_cod, sala_cod, empleadono, apellido)
values (2,22,1234,'Garcia J');
En esta inserci�n no se contemplan todos los campos de la
tabla, )Falla la inserci�n?.
No*//

//*63. Cambiar alpaciente (tabla ENFERMO) n�mero 74835 la direcci�n a Alcala 411.*//

update enfermo
set direccion = 'Alcala 411'
where inscripcion = 74835

//*64. Poner todas las direcciones de la tabla ENFERMO a null.*//

update enfermo
set direccion = null

//*65. Igualar la direcci�n y fecha de nacimiento del paciente 10995 a los valores de las
columnas correspondientes almacenadas para el paciente 14024.*//

UPDATE enfermo
SET direccion = (SELECT direccion FROM enfermo WHERE inscripcion = 14024),
    fecha_nac = (SELECT fecha_nac FROM enfermo WHERE inscripcion = 14024)
WHERE inscripcion = 10995;

//*66. En todos los hospitales del pa�s se ha recibido un aumento del presupuesto, por lo que se
incrementar� el n�mero de camas disponibles en un 10%. )Como se har�a en SQL?.*//

Habr�a que cambiar la estructura de la tabla pues es posible que cambie la
longitud del campo.
Modificamos con alter y despu�s con update
spool upd4
create table hospitales2
as select * from hospital
alter table hospitales2
modify num_cama number(4)
update hospitales2
set num_cama = num_cama + (num_cama*0.1)

//*67.-Ejercicio a comentar su soluci�n.*//

create table hospital22 as
select * from hospital where 1 = 2;

alter table hospital22
add hospital_pk number(2) primary key;

create table plantilla22 (
    hospital_cod number(2) not null,
    sala_cod number(2) not null,
    empleado_no number(4),
    apellido varchar2(15),
    funcion char(10),
    turno char(1),
    salario number(10),
    constraint empleado_pk primary key (empleado_no),
    constraint hospital_fk foreign key (hospital_cod) references hospital22(hospital_cod),
    constraint sala_fk foreign key (sala_cod, hospital_cod) references sala(sala_cod, hospital_cod),
    constraint turno_ch check (rtrim(upper(turno)) in ('T', 'M', 'N')),
    constraint salario_ch check (nvl(salario, 0) >= 0)
);

//*68.- Rellenar la tabla HOSPITALES22 con las filas de HOSPITAL. Da esto alg�n problema?
Por que?*//

insert into hospitales22
select * from hospital

--Repetir la operaci�n? DA PROBLEMAS. DUPLICIDAD DE CLAVES.

//*69. Crearse una tabla llamada VARONES con la misma estructura que la tabla enfermo.*//

CREATE TABLE VARONES (
  inscripcion NUMBER(5) NOT NULL,
  apellido VARCHAR2(25),
  direccion VARCHAR2(12),
  fecha_nac DATE,
  sexo CHAR(1),
  nss NUMBER(9)
);

//*70. Crear la tabla EMPLEADOS con la misma estructura que la tabla Emp y conteniendo los
datos de oficio PRESIDENTE o comisi�n mayor que el 25% del salario.*//

create table empleados
as
select *
from emp
where upper(oficio) = 'DIRECTOR' or
comisi�n > 0.25*salario

CREACI�N DE VISTAS

//*71. - Crear una vista para los departamentos 10 y 20.
- Crear una vista para los departamentos 10 y 30.
- Hacer una JOIN de las dos vistas anteriores.*//

create view emp10 as
select *
from emp
where dept_no in (10,20)
create view emp30 as
select *
from emp
where dept_no in (10,30)
select e1.dept_no, e2.dept_no, e1.apellido, e2.oficio,
e1.salar�o, nvl(e1.comision,O)
from emp10 e1, emp30 e2
where e1.dept_no = e2.dept_no

//*72. Hacer una JOIN de la tabla DEPT2 y la vista de los departamentos 10 y 20.*//
select e1.dept_no, e2.dept_no, e1.apellido, e1.oficio,
e1.salario, nvl(e1.comision,0)
from emp10 e1, emp e2
where e1.dept_no = e2.dept_no
//*73. Se va a realizar un programa de consulta de la informaci�n sobre enfermos. Los datos a
mostrar ser�n sus apellidos, direcci�n, fecha de nacimiento y hospital en el que se encuentran.
)Qu� vista se definir�?. )Es posible modificar datos a trav�s de la vista anterior?.*//
create view enferm
as select e.apellido, e.direccion, e.fecha_nac, h.nombre
from enfermo e, hospital h, ocupaci�n o
where e.inscripcion = o.inscripcion and
o.hospital_cod = h.hospital-cod

Seria posible modificar datos a trav�s de la vista creada anteriormente? )Por que?
NO, POR MANEJAR M�LTIPLES TABLAS. SI SE PODR�A CON UNA
SOLA.

//*74. create view emp_cua
as select dept_no, sum(salario) salariototal
from emp
Estudiar esta vista.*//

create view emp_cua
as select dept-no,sum(salario) salariototal
from emp
group by dept_no

//*75. Crear una vista para el departamento 10 con la cl�usula with check option. )Qu� ocurre?.*//

create view emp_ter
as select * from emp
where dept_no = 10
with check option
