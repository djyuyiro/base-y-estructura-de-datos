USE ejercicio_02

//*CL�USULA WHERE*//

1. Encuentre a todos los miembros del personal cuyo nombre empiece por 'H'.

select apellido
from plantilla
where upper(apellido) like 'H%';

2. Quienes son las enfermeras y enfermeros que trabajan en turnos de Tarde o Ma�ana?

select apellido
from plantilla
where upper(funcion) in ('ENFERMERO' ,'ENFERMERA')

and upper(turno) in ('T','M');

3. Haga un listado de las enfermeras que ganan entre 2.000.000 y 2.500.000 Pts.

select apellido, salario
from plantilla
where salario between 2000000 and 2500000
and upper(funcion)= 'ENFERMERA';

4. Mostrar, para todos los hospitales, el c�digo de hospital, el nombre completo del hospital
y su nombre abreviado de tres letras (a esto podemos llamarlo ABR) Ordenar la recuperaci�n por
esta abreviatura.

SELECT hospital_cod, nombre, SUBSTRING(nombre, 1, 3) AS abr
FROM hospital
ORDER BY abr;

//* 5. En la tabla DOCTOR otorgar a Cardiolog�a un c�digo de 1, a Psiquiatr�a un c�digo de 2,
a Pediatr�a un c�digo de 3 y a cualquier otra especialidad un c�digo de 4. Recuperar todos los
doctores, su especialidad y el c�digo asignado.*//

SELECT apellido, especialidad,
  CASE especialidad
    WHEN 'Cardiolog�a' THEN '1'
    WHEN 'Psiquiatr�a' THEN '2'
    WHEN 'Pediatr�a' THEN '3'
    ELSE '4'
  END AS codigo
FROM DOCTOR;


//*6. Hacer un listado de los nombres de los pacientes y la posici�n de la primera letra 'A' que
aparezca en su apellido, tomando como referencia la primera letra del mismo*//

SELECT apellido, CHARINDEX('A', apellido) AS "PRIMERA LETRA A"
FROM enfermo;



//*7.Queremos conseguir:
COMENTARIO
--------------------
El departamento de CONTABILIDAD esta en SEVILLA
El departamento de INVESTIGACI�N esta en MADRID
El departamento de VENTAS esta en BARCELONA
El departamento de PRODUCCI�N esta en BILBAO*//

SELECT CONCAT('El departamento de ', dnombre, ' est� en ', loc) AS COMENTARIO
FROM dept2;



//*8. Para cada empleado cuyo apellido contenga una "N", queremos que nos devuelva "nnn",
pero solo para la primera ocurrencia de la "N". La salida debe estar ordenada por apellido
ascendentemente.*//

SELECT
  CASE
    WHEN CHARINDEX('N', apellido, 1) > 0 THEN
      SUBSTRING(apellido, 1, CHARINDEX('N', apellido, 1) - 1) + 'nnn' + SUBSTRING(apellido, CHARINDEX('N', apellido, 1) + 1, LEN(apellido))
    ELSE apellido
  END AS "TRES N"
FROM emp
WHERE UPPER(apellido) LIKE '%N%'
ORDER BY apellido;

//*9. Para cada empleado se pide que salga su salario total (salario mas comisi�n) y luego su
salario fragmentado, es decir, en centenas de mil, decenas de mil... decenas y unidades. La salida
debe estar ordenada por el salario y el apellido descend�ntemente.*//

SELECT
  apellido,
  salario + COALESCE(comision, 0) AS sal_total,
  FLOOR((salario + COALESCE(comision, 0)) / 100000) AS c,
  FLOOR(((salario + COALESCE(comision, 0)) % 100000) / 10000) AS d,
  FLOOR(((salario + COALESCE(comision, 0)) % 10000) / 1000) AS m,
  FLOOR(((salario + COALESCE(comision, 0)) % 1000) / 100) AS c1,
  FLOOR(((salario + COALESCE(comision, 0)) % 100) / 10) AS d1,
  (salario + COALESCE(comision, 0)) % 10 AS u
FROM emp
ORDER BY sal_total DESC, apellido DESC;

//*10. Para cada empleado que no tenga comisi�n o cuya comisi�n sea mayor que el 15% de su salario, se pide el salario total que tiene. Este ser�: si tiene comisi�n su salario mas su comisi�n,
y si no tiene, su salario mas su nueva comisi�n (15% del salario). La salida deber� estar ordenada por el oficio y por el salario que le queda descend�ntemente.*//

SELECT apellido AS APELLIDO, oficio AS OFICIO,
  salario + COALESCE(comision, salario * 0.15) AS "SALARIO TOTAL"
FROM emp
WHERE oficio IN ('ANALISTA', 'DIRECTOR', 'EMPLEADO', 'PRESIDENTE', 'VENDEDOR')
ORDER BY "SALARIO TOTAL" DESC;

//* 11. Encuentre a todas las enfermeras y enfermeros con indicaci�n del salario mensual de cada
uno.*//

SELECT apellido, ROUND(salario / 12, 0) AS "SALARIO MENSUAL"
FROM plantilla
WHERE UPPER(funcion) IN ('ENFERMERA', 'ENFERMERO');

//* 12. Que fecha fue hace tres semanas?*//

SELECT DATEADD(WEEK, -3, GETDATE()) AS fecha;

//* 13. Se pide el nombre, oficio y fecha de alta del personal del departamento 20 que ganan mas de
150000 ptas. mensuales.*//

SELECT apellido, oficio, 
  CONVERT(NVARCHAR, fecha_alta, 106) AS alta
FROM nombre_de_la_tabla
WHERE (dept_no = 20) AND (salario > 150000);

//*14. Se pide el nombre, oficio y el d�a de la semana en que han sido dados de alta los
empleados de la empresa, pero solo de aquellos cuyo d�a de alta haya sido entre martes y jueves.
Ordenado por oficio.*//

SELECT emp_no, oficio, DATE_FORMAT(fecha_alt, '%W') AS d�a
FROM emp
WHERE DATE_FORMAT(fecha_alt, '%a') IN ('Tue', 'Wed', 'Thu')
ORDER BY oficio;

//* 15. Para todos los empleados, el d�a en que fueron dados de alta en la empresa debe estar
ordenada por el d�a de la semana (Lunes, Martes ... Viernes) . Los d�as no laborables ser�n "Fin
de semana".*//

SELECT apellido, oficio, 
  CASE 
    WHEN DATEPART(WEEKDAY, fecha_alt) = 1 THEN 'Domingo'
    WHEN DATEPART(WEEKDAY, fecha_alt) = 2 THEN 'Lunes'
    WHEN DATEPART(WEEKDAY, fecha_alt) = 3 THEN 'Martes'
    WHEN DATEPART(WEEKDAY, fecha_alt) = 4 THEN 'Mi�rcoles'
    WHEN DATEPART(WEEKDAY, fecha_alt) = 5 THEN 'Jueves'
    WHEN DATEPART(WEEKDAY, fecha_alt) = 6 THEN 'Viernes'
    ELSE 'Fin de semana'
  END AS dia_de_la_semana
FROM emp
ORDER BY dia_de_la_semana;
























